TEMPLATE = app
CONFIG += c++11
CONFIG -= app_bundle
SOURCES += main.cpp \
    dynarray.cpp \
    mylist.cpp

HEADERS += \
    dynarray.h \
    mylist.h \
    illegal.h
