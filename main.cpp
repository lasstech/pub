#include <iostream>
#include <cassert>
#include <dynarray.h>
#include <mylist.h>
#include <vector>
#include <algorithm>

using namespace std;


template<typename T>
class ListMed
{
private:

};

template<typename T>
void microTestList(int start, int range, T a[] ){
    auto list = new MyList<T>();
    auto vec = vector<T>();
    for (int i = start; i < range; ++i) {
        vec.push_back(a[i]);
        list->sortOnTheFly(a[i]);
    }
    std::sort(vec.begin(), vec.end());
    list->printList();

    list->setCurrent(list->getBegin());
    bool same = true;
    for (int i = 0; i < range-start; ++i) {
        if(vec[i]!=list->getCurrVal()){
            same = false;
            break;
        }
        list->goToNext();
    }
    same?cout<<" correct":cout<<" wrong sort";
    cout<<"\n";
    delete list;
}

template<typename T>
void microTestArray(int start, int range, T a[] ){
    auto med = new DynArray<T>();
    auto vec = vector<T>();
    for (int i = start; i < range; ++i) {
        vec.push_back(a[i]);
        med->medianOnTheFly(a[i]);
    }
    std::sort(vec.begin(),vec.end());
    med->printArray();


    bool same = true;
    for (int i = 0; i < range-start; ++i) {
        if(vec[i]!=med->getItemAt(i)){
            same = false;
            break;
        }
    }
    same?cout<<" correct":cout<<" wrong sort";
    cout<<"\n";
    delete med;
}


template<typename T>
void test(){
    int range =12;
    int mux = 10;
    int pool = range*mux;
    T a[pool];
    for (int i = 0; i < pool; ++i) {
        a[i]=((T)(rand()%100))/10;
    }
    cout<<"list\n";
    for (int i = 0; i < mux; ++i) {
        microTestList(i*range,range*(i+1),a);
    }
    cout<<"array\n";
    for (int i = 0; i < mux; ++i) {
        microTestArray(i*range,range*(i+1),a);
    }
}

int main()
{
    test<double>();


    return EXIT_SUCCESS;
}
