#ifndef LIST_H
#define LIST_H

template<typename T>
class MyList
{
private:
    class ListElement
    {
    public:
        ListElement(){
            previous =nullptr;
            next =nullptr;
        }
        ListElement *getNext() const{           return next;}
        ListElement *getPrevious() const{       return previous;}
        T getVal() const{               return val;}
        void setVal(const T &value){    val = value;}


        void setNext(ListElement *value){
            value->next = next;
            value->previous= this;
            if (next!= nullptr)
                next->previous = value;
            next = value;
        }
        void setPrevious(ListElement *value){
            value->previous = previous;
            value->next= this;
            if (previous!= nullptr)
                previous->next = value;
            previous = value;
        }

    private:
        T val;
        ListElement *previous;
        ListElement *next;

    };
private:
    ListElement *m_begin;
    ListElement *m_end;
    ListElement *m_current;
    ListElement *m_middle;
    int m_size;
    int middlePos;
    float median;
public:
    MyList();
    virtual~MyList();
    ListElement *getBegin() const;
    ListElement *getEnd() const;
    ListElement *getCurrent() const;
    void goToNext();
    void goToPrev();
    T getCurrVal() const;
    T getPrevVal() const;
    T getNextVal() const;
    float sortOnTheFly(T val);
    void insertAfter(T val);
    void insertAfter(T val,  ListElement *el);
    void insertBefore(T val);
    void insertBefore(T val,  ListElement *el);
    void clearList();
    void printList();
    void append(T val);
    ListElement *getMiddle() const;

    int getMiddlePos() const;

    void setCurrent(ListElement *current);

    float getMedian() const;

private:
    void setBegin(ListElement *begin);
    void setEnd(ListElement *end);
    void initialise();
    void setMiddle(int middle);
    void searchUp(T val);
    void searchDown(T val);
    float calcMedian();
    ListElement *initInsert(T val,  ListElement *refEl);
};

#endif // LIST_H
