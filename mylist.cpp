#include "mylist.h"
#include <iostream>

template<typename T>
void MyList<T>::setCurrent(ListElement *current){
    m_current = current;
}

template<typename T>
float MyList<T>::getMedian() const
{
    return median;
}

template<typename T>
MyList<T>::MyList()
{
    initialise();
}

template<typename T>
MyList<T>::~MyList()
{
    clearList();
}

template<typename T>
typename MyList<T>::ListElement *MyList<T>::getBegin() const
{
    return m_begin;
}

template<typename T>
typename MyList<T>::ListElement *MyList<T>::getEnd() const
{
    return m_end;
}

template<typename T>
void MyList<T>::setBegin(ListElement *begin)
{
    m_begin = begin;
}

template<typename T>
void MyList<T>::setEnd(ListElement *end)
{
    m_end = end;
}

template<typename T>
typename MyList<T>::ListElement *MyList<T>::getCurrent() const
{
    return m_current;
}

template<typename T>
void MyList<T>::goToNext()
{
    if (m_current->getNext()!=nullptr){
        m_current = m_current->getNext();
    }
}

template<typename T>
void MyList<T>::goToPrev()
{
    if (m_current->getPrevious()!=nullptr){
        m_current = m_current->getPrevious();
    }
}

template<typename T>
T MyList<T>::getCurrVal() const
{
    return m_current->getVal();
}

template<typename T>
T MyList<T>::getPrevVal() const
{
    return m_current->getPrevious()->getVal();
}

template<typename T>
T MyList<T>::getNextVal() const
{
    return m_current->getNext()->getVal();
}

template<typename T>
float MyList<T>::sortOnTheFly(T val)
{
    if (m_size>2){
        m_current=getMiddle();
        if ((val<=getNextVal())&&(val>=getPrevVal())){
                val>getCurrVal() ? insertAfter(val) : insertBefore(val);
        }
        else if (val>getNextVal()){
            searchDown(val);
        }
        else{
            searchUp(val);
        }
    }
    else if (m_size ==2){
        (val >=getEnd()->getVal()) ?
                    insertAfter(val, getEnd()) :
                    val <=getBegin()->getVal() ?
                         insertBefore(val, getBegin()) :
                         insertAfter(val, getBegin());
        m_middle = getBegin()->getNext();
    }
    else if(m_size==1){
        val>=getCurrVal() ? insertAfter(val) : insertBefore(val);
    }
    else if (m_size==0){
        insertAfter(val);
        m_current=getMiddle();
    }
    return calcMedian();
}

template<typename T>
void MyList<T>::searchUp(T val){
    goToPrev();
    while (m_current!=getBegin()) {
        if (val>=getPrevVal()){
            insertBefore(val);
            break;
        }
        goToPrev();
    }
    if(m_current==getBegin())
        insertBefore(val);
}

template<typename T>
void MyList<T>::searchDown(T val)
{
    goToNext();
    while (m_current!=getEnd()) {
        if (val<=getNextVal()){
            insertAfter(val);
            break;
        }
        goToNext();
    }
    if(m_current==getEnd())
        insertAfter(val);
}

template<typename T>
float MyList<T>::calcMedian()
{
    if(middlePos==0)
        median =(float)m_middle->getVal();
    else if (middlePos==-1)
        median =(float)( m_middle->getVal() + m_middle->getPrevious()->getVal())/2;
    else if (middlePos==1)
        median = (float)( m_middle->getVal() + m_middle->getNext()->getVal())/2;
    return median;
}

template<typename T>
void MyList<T>::clearList()
{
    m_current = getBegin();
    goToNext();
    while (m_current!=getEnd()){
        delete m_current->getPrevious();
        goToNext();
    }
    delete m_current;
    initialise();
}

template<typename T>
void MyList<T>::printList()
{
    auto backup_current = m_current;
    std::cout<<"[";
    m_current = getBegin();
    while (m_current!=getEnd()){
        std::cout<<getCurrVal()<<", ";
        goToNext();
    }
    std::cout<<getCurrVal()<<"] "<<m_middle->getVal()<<" "<< median;
    m_current = backup_current;

}

template<typename T>
void MyList<T>::append(T val)
{
    insertAfter(val,getEnd());
    m_current = getEnd();
}


template<typename T>
typename MyList<T>::ListElement *MyList<T>::getMiddle() const
{
    return m_middle;
}

template<typename T>
void MyList<T>::setMiddle(int middle)
{
        middlePos+=middle;
        if(middlePos==-2){
            middlePos= 0;
            m_middle = (getMiddle()->getPrevious());
        }
        else if(middlePos==2){
            middlePos= 0;
            m_middle = (getMiddle()->getNext());
        }
}

template<typename T>
int MyList<T>::getMiddlePos() const
{
    return middlePos;
}

template<typename T>
void MyList<T>::initialise()
{
    m_current   = nullptr;
    m_begin     = nullptr;
    m_end       = nullptr;
    m_middle    = nullptr;
    middlePos   = 0;
    m_size      = 0;
}

template<typename T>
typename MyList<T>::ListElement *MyList<T>::initInsert(T val, MyList::ListElement *refEl)
{
    ListElement *el = new ListElement();
    el->setVal(val);
    if (refEl ==nullptr){
        refEl = el;
        setBegin(el);
        setEnd(el);
        m_middle =el;
    }
    return el;
}

template<typename T>
void MyList<T>::insertAfter(T val, ListElement *refEl)
{
    auto el = initInsert(val,refEl);
    if (refEl !=nullptr) {
        refEl->setNext(el);
        if (refEl==getEnd()){
            setEnd(el);
        }
        setMiddle(+1);
    }
    m_size++;
}

template<typename T>
void MyList<T>::insertAfter(T val)
{
    insertAfter(val,m_current);
}

template<typename T>
void MyList<T>::insertBefore(T val, ListElement *refEl)
{
    auto el = initInsert(val,refEl);
    if (refEl !=nullptr) {
        refEl->setPrevious(el);
        if (refEl==getBegin()){
            setBegin(el);
        }
        setMiddle(-1);
    }
    m_size++;
}

template<typename T>
void MyList<T>::insertBefore(T val)
{
    insertBefore(val,m_current);
}

template class MyList<float>;
template class MyList<double>;
template class MyList<int>;
