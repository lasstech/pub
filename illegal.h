#ifndef STRUCTURED_H
#define STRUCTURED_H

#include <vector>
#include <list>

template<typename T>
inline float medianFromList(T myList){
    float median;
    if (myList.size()%2)
        median = (float) *std::next(myList.begin(), myList.size()/2);
    else
    {
        auto incr = std::next(myList.begin(), (myList.size()/2)-1);
        median = (float) (*incr + *std::next(incr))/2;
    }
    return median;
}

template<typename T>
auto fromStart_List(T i) -> const list<T>*{
    static auto myList = new list<T>;

    if (myList.empty())
        myList.push_back(i);

    else{
        auto iter = myList.begin();
        while(1){
            if (*iter>=i){
                myList.insert(iter,i);
                break;
            }
            iter = std::next(iter);
            if (iter==myList.end()){
                myList.push_back(i);
                break;
            }
        }
    }
    return medianFromList<list<T>>(myList);
}


template<typename T>
auto geometricalHalf_vector(T i) -> const vector<T>* {
    static auto myVec = new vector<T>;
    auto iter = myVec->begin();
    int vLen = myVec->size();
    if (vLen==0)
        myVec->push_back(i);
    else if(vLen==1){
        (*myVec)[0]<i ? myVec->push_back(i) : (void) myVec->insert(iter,i);
    }
    else{
        if (i<(*myVec)[0])             myVec->insert(iter,i);
        else if (i>(*myVec)[vLen-1])   myVec->push_back(i);
        else {
            int vIndex, vInterval;
            vIndex = vInterval = vLen>>1;
            while(1){
                auto valL = (*myVec)[vIndex];
                auto valH = (*myVec)[vIndex+1];

                if ((i <= valH)&&(i>=valL)) {
                    myVec->insert(iter+vIndex+1,i);
                    break;
                }
                vInterval =vInterval>>1;
                i > valH ? vIndex +=vInterval : vIndex = vIndex>>1;
            }
        }
    }
    return myVec;
}





#endif // STRUCTURED_H
