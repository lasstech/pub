#include "dynarray.h"


template<typename T>
DynArray<T>::DynArray(){
    m_size=0;
}

template<typename T>
DynArray<T>::~DynArray()
{
    delete[] m_pArray;
}

template<typename T>
T DynArray<T>::getItemAt(int i)
{
    return *(m_pArray+i);
}

template<typename T>
int DynArray<T>::getSize(){
    return m_size;
}

template<typename T>
void DynArray<T>::pushBack(T val){
    insert(val, m_size);
}

template<typename T>
float DynArray<T>::getMedian() const
{
    return median;
}


template<typename T>
void DynArray<T>::p_copy(T *temp, int range, int &current, int after_index){
    while(current<range){
        *(temp+current) = getItemAt(current-after_index);
        ++current;
    }
}

template<typename T>
void DynArray<T>::insert(T val, int index){
    m_size++;
    auto temPtr = new T[m_size];
    if(m_size==1){
        m_pArray =temPtr;
        *m_pArray = val;
    }
    else {
        int i=0;
        p_copy(temPtr, index-1, i);
        if(index<m_size){
            p_copy(temPtr, index, i);
            *(temPtr+i++) = val;
            p_copy(temPtr, m_size, i, 1);
        } else if(index==m_size){
            *(temPtr+i) = val;
        }
        else{
            cout<<"index to big";
        }
        delete[] m_pArray;
        m_pArray = temPtr;
    }
}

template<typename T>
float DynArray<T>::medianOnTheFly(const T value) {

    if (m_size==0)
        insert(value);
    else if(m_size==1){
        getItemAt(0)>value ?
                    insert(value) :
                    pushBack(value);
    }
    else if (m_size ==2){
        getItemAt(0)>value ?
                    insert(value) :
                    (getItemAt(1)>value ?
                         insert(value,1) :
                         pushBack(value));
    }
    else{
        value<(getItemAt(0)) ?
                    insert(value) :
                    (value>getItemAt(m_size-1) ?
                         pushBack(value) :
                         geometricFit(value));
    }
    return medianCounter();
}

template<typename T>
float DynArray<T>::medianCounter(){
    if (m_size%2){
        median = (float)getItemAt(((m_size)/2));
        return median;
    }
    else {
        median = ((float)getItemAt(-1+(m_size>>1))+(float)getItemAt(m_size>>1))/2;
        return median;
    }
}

template<typename T>
void DynArray<T>::geometricFit(T value)
{
    int vIndex, vInterval;
    vIndex = vInterval = m_size>>1;
    while(1){
        auto valH = getItemAt(vIndex+1);
        auto valL = getItemAt(vIndex);
        if ((value <= valH)&&(value>=valL)) {
            insert(value, vIndex+1);
            break;
        }
        vInterval =max((vInterval>>1),1);                                   //numerical
        value > valH ? vIndex +=vInterval : vIndex -=vInterval;
    }
}

template<typename T>
void DynArray<T>::printArray()
{
    cout<<"[";
    for (int i = 0; i < m_size-1; ++i) {
        cout<<getItemAt(i)<<", ";
    }
    cout<<getItemAt(m_size-1)<<"]";
    cout<<median<<" ";
}

template class DynArray<float>;
template class DynArray<double>;
template class DynArray<int>;
