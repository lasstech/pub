#ifndef DYNARRAY_H
#define DYNARRAY_H

#include <iostream>

using namespace std;

template< typename T >
class DynArray
{
private:
    int m_size;
    float median;
public:
    T *m_pArray;

private:
    void p_copy(T* temp,int range, int &current, int after_index=0);

public:
    DynArray();
    virtual ~DynArray();
    void pushBack(T val);
    void insert(T val,  int index=0);
    int getSize();
    float medianOnTheFly(const T value);
    float medianCounter();
    T getItemAt(int i);
    void geometricFit(T value);
    void printArray();
    float getMedian() const;
    void setMedian(float value);
};





#endif // DYNARRAY_H
